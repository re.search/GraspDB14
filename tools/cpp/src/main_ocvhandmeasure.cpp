/** main_ocvmeasurehand -  A one line description of the file.
 *
 * #include "xx.h" <BR>
 * -llib opencv
 *
 * A longer description.
 *
 * @see something
 *  Created on: Nov, 2014
 *      Author: kstoll2m
 *
 *
 * Usage: ./<name-of-program>
 * click into the image to add joint image coordinates
 * press 'l' load another image (*-rect.jpg)
 *       's' save clicked points (should be 26 points)
 *       'h' help (displays this information)
 *       <backspace> remove one point
 *       <esc> end the program
 */




#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm> /*std::reverse*/

#ifdef _WIN32
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#else
#include <opencv/highgui.h>
#endif

#include "filechooser_class.h"
#include "filename_class.h"
#include "filestorageocv_class.h"


double SCALEFACTOR;


// easy switching between float and double
typedef double real;
namespace cv {
  typedef Point2d Point2x;
  typedef Vec2d   Vec2x;
}



void mouseCb ( int, int, int, int, void* );
void loadRectifiedHandImage ( FileName&, cv::Mat&, double& );
void saveClickedPoints ( FileName& , const std::vector<cv::Point2x >& );
void printHelp ( void );





int main ( int argc, char** argv ) {


  cv::Mat        image, originalimage;
  FileName       xmlfilename;
  bool           quit = false;
  bool           saved = false;
  std::string    bildgeladen;

  // interaction related
  std::vector< cv::Point2x >            clickedpoints;

  bildgeladen = "No image could be loaded";

  loadRectifiedHandImage ( xmlfilename, image, SCALEFACTOR );
  bildgeladen = "Image name: " + xmlfilename.getName();
  if ( SCALEFACTOR > 0 ) {
    originalimage = image.clone();
  }


  cv::namedWindow( bildgeladen, CV_WINDOW_NORMAL+CV_GUI_EXPANDED+CV_WINDOW_KEEPRATIO);
  //cv::setWindowProperty( bildgeladen, CV_WINDOW_FULLSCREEN, true );

  cv::setMouseCallback( bildgeladen, mouseCb, &clickedpoints );

  printHelp();



  while ( !quit ) {
    // detect closing the window with the mouse (this will delete the mouse callback)
    if (!cvGetWindowHandle( bildgeladen.c_str() ) ) {
      cv::namedWindow( bildgeladen, CV_WINDOW_NORMAL+CV_GUI_EXPANDED+CV_WINDOW_KEEPRATIO);
      cv::setMouseCallback( bildgeladen, mouseCb, &clickedpoints );
    }

    if ( !image.empty()) {

      for ( size_t i=0; i<clickedpoints.size(); ++i ) {
        std::string text = std::to_string(i);
        int fontface = cv::FONT_HERSHEY_PLAIN;
        double fontscale = 1.25;
        int thickness = 1;
        int baseline = 0;
        cv::Vec3b color = originalimage.at<cv::Vec3b>(clickedpoints[i]);
        cv::Scalar invcolor(255-color[0], 255-color[1], 255-color[2]);
        cv::Scalar black = cv::Scalar::all(0);
        cv::Scalar white(255,255,255,0);
        int radius = 3;

        if (i>0 /*&& i < 10*/ ) {
         // if ( i%2 )
          if ( i!=5 && i!=10 && i!=15 && i!=20 && i!=24 )
            cv::line ( image, clickedpoints[i-1], clickedpoints[i], black, 1 );
        }
        cv::circle ( image, clickedpoints[i], radius, invcolor );
        /*cv::circle ( image, center, radius, color, int thickness=1, int lineType=8, int shift=0 );*/
        cv::Size textsize = cv::getTextSize(text, fontface, fontscale, thickness, &baseline);
        baseline+=thickness;
        // add transparent box start
        cv::Rect roirect(clickedpoints[i]+cv::Point2x(10,baseline),clickedpoints[i]+cv::Point2x(10+textsize.width,-textsize.height-baseline));
        cv::Mat roi = image(roirect);
        cv::Mat roicolor(roi.size(), CV_8UC3, black);
        double alpha = 0.75;
        cv::addWeighted(roicolor, alpha, roi, 1.0-alpha, 0.0, roi);
        // add transparent box end
        cv::putText( image, text, clickedpoints[i]+cv::Point2x(10,0), fontface, fontscale, white, thickness);
      }

      cv::imshow ( bildgeladen, image );


    }

    

    char k = cv::waitKey(5);
    switch (k) {
    case 27 :
      quit = true;
      break;
    case 'l' :
      /*
      bool reallyload = false;
      if ( !saved ) {
        std::cout << "You did not save the points, are you sure you want to load a new image? [y/N] ";
        char yesorno;
        std::string yesornoline;
        std::getline(std::cin, yesornoline);
        if (yesornoline.empty()) yesorno = 'n';
        else if (yesornoline.size() > 1) yesorno = 'n';
        else yesorno = yesornoline[0];
        switch (yesorno) {
          case 'y' :
          case 'Y' :
            std::cout << "you entered " << yesorno << "es" << std::endl;
            reallyload = true;
            break;
          default :
            std::cout << "you entered " << yesorno << "o" << std::endl;
            break;
        }
      } else {
      }
      */
      loadRectifiedHandImage ( xmlfilename, image, SCALEFACTOR );
      if ( SCALEFACTOR > 0 ) {
        originalimage = image.clone();
        clickedpoints.clear();
        saved = false;
      }

      break;
    case 8 :
      std::clog << "removing last point from clicked points" << std::endl;
      if (clickedpoints.size()>0) clickedpoints.pop_back();
      break;
    case 's' :
      if ( clickedpoints.size()<26 ) {
        std::cerr << "unable to save, not all points present" << std::endl;
        break;
      }
      if ( clickedpoints.size()>26 ) {
        std::cerr << "unable to save, more points than expected" << std::endl;
        break;
      }
      saveClickedPoints( xmlfilename, clickedpoints );
      saved = true;
      break;
    case 'h' :
      printHelp();
      break;
    default:
      break;
    }

    image = originalimage.clone();
}

  cv::destroyAllWindows();

  return EXIT_SUCCESS;

} // endof main



//-----------------------------------------------------------------------------
void 
printHelp ( void ) {
  
  std::cout <<
    "\n" <<
    "  You asked for help so help shall be provided.\n" <<
    "    There are the following ways of interaction: \n" <<
    "    Click into the image to add joint image coordinates.\n" <<
    "    Press 'l' to load another image (*-rect.jpg)\n" <<
    "          's' to save clicked points (should be 26 points)\n" <<
    "          'h' to help (displays this information)\n" <<
    "          <backspace> to remove one point.\n" <<
    "          <esc> to end the program\n" <<
    std::endl;

} // endof printHelp




//-----------------------------------------------------------------------------
void
mouseCb ( int event, int x, int y, int flags, void* ptr ) {

  if (event == cv::EVENT_FLAG_LBUTTON ) {
    std::vector< cv::Point2x >* ptr_clickedpoint = (std::vector< cv::Point2x > *) ptr;
    cv::Point p;
    float dist = 0;
    p.x = x;
    p.y = y;
    ptr_clickedpoint->push_back(p);
    if (ptr_clickedpoint->size()>1) {
      cv::Vec2x p1(p.x, p.y);
      cv::Vec2x p2(ptr_clickedpoint[0][ptr_clickedpoint->size()-2]);
      dist = static_cast< float >( cv::norm( p1, p2, CV_L2 ) );
    }
    std::cout << " [" << std::setw(4) <<  p.x << ", " << std::setw(4) << p.y << "] "
        << std::setw(6) << dist << "/" << SCALEFACTOR << "="
        << dist/SCALEFACTOR*1. << std::endl;
  }
} // endof mouseCb





//-----------------------------------------------------------------------------
void
loadRectifiedHandImage ( FileName& ref_filename, cv::Mat& ref_image, double& ref_scalefactor ) {

  FileChooser    imagename;
  std::string    imagetoload;
  FileStorageOcv fso;


  imagename.setDefaultDirectory("$HOME/");
  imagename.chooseFile("-rect.jpg");
  imagetoload = imagename.getFile();
//  imagetoload = "/home/kstoll2m/PhD/native/subject01/img20140911155734-hand-rect.jpg";
  if ( imagetoload.empty() ) {
    ref_scalefactor = -1;
    printHelp();
    return;
  }
  ref_filename = FileName(imagetoload);
  ref_filename.setExtension("xml");
  ref_scalefactor = fso.loadHomographyScale( ref_filename.getCompleteFileName() );

  ref_image = cv::imread( imagetoload );

} // endof loadRectifiedHandImage





//-----------------------------------------------------------------------------
void
saveClickedPoints ( FileName& ref_filename, const std::vector<cv::Point2x >& ref_clickedpoints ) {

  std::cout << "saving points" << std::endl;

  std::vector<cv::Point2x> rawpointsreversed;
  std::vector<cv::Point2x> scaledpointsreversed;
  std::vector<real> rawlengths;
  std::vector<real> scaledlengths;
  real dist = 0;
  double scalecorrection = 1./SCALEFACTOR;
  cv::FileStorage   ocvfs;

  // make sure the arrays are empty
  scaledpointsreversed.clear();
  scaledpointsreversed.clear();
  rawlengths.clear();
  scaledlengths.clear();



  // the click order is exacltly the inverse of the defined name map
  // to be conform with the name map (also see lengthy comments below)
  for ( int i=ref_clickedpoints.size()-1; i>=0; --i ) {
    rawpointsreversed.push_back( ref_clickedpoints[i] );
    scaledpointsreversed.push_back( ref_clickedpoints[i]*scalecorrection );
  }

  // calculate distances for each
  for ( size_t i=1; i<ref_clickedpoints.size(); ++i ) {
    if ( i!=5 && i!=10 && i!=15 && i!=20 ) {
      cv::Vec2x p1( ref_clickedpoints[i-1] );
      cv::Vec2x p2( ref_clickedpoints[i] );
      dist = cv::norm (p1, p2, CV_L2 );
    } else {
      cv::Vec2x p1( ref_clickedpoints[i-1] );
      cv::Vec2x p2( ref_clickedpoints[24] );
      dist = cv::norm (p1, p2, CV_L2 );
    }
    rawlengths.push_back( dist );
    scaledlengths.push_back( dist*scalecorrection );
  }

  // length were calculated on the clicked order, so these need to be reversed too
  std::reverse(rawlengths.begin(), rawlengths.end());
  std::reverse(scaledlengths.begin(), scaledlengths.end());



  // write everything to the (already existant) xml-file
  ocvfs.open(ref_filename.getCompleteFileName(), cv::FileStorage::APPEND);

  ocvfs << "hand";
  ocvfs << "{";

  ocvfs << "right";
  ocvfs << "{";

  ocvfs << "image_based_measurement";
  ocvfs << "{";

  ocvfs << "raw";
  ocvfs << "{";
  ocvfs << "coordinates_2d" << rawpointsreversed;
  ocvfs << "lengths" << rawlengths;
  ocvfs << "}"; // end raw;

  ocvfs << "scaled";
  ocvfs << "{";
  ocvfs << "coordinates_2d" << scaledpointsreversed;
  ocvfs << "lengths" << scaledlengths;
  ocvfs << "}"; // end scaled;

  ocvfs << "}"; // end image_based_measurements

  ocvfs << "}"; // end right;

  ocvfs << "}"; // end hand;

  ocvfs.release();


  /* joints (clicked order -idxB- and namemap -idxA-)
   *
   * idxB | idxA | name     | description
   * -----+------+----------+----------------------------------------
   *   25 |    0 | -------- | not present
   *   24 |    1 | root     | root / wrist
   *   23 |    2 | th-tmcj  | Thumb  trapeziometacarpal joint
   *   22 |    3 | th-mcpj  | Thumb  metacarpal joint
   *   21 |    4 | th-ipj   | Thumb  interphalangeal joint
   *   20 |    5 | th-tip   | Thumb  tip
   *   19 |    6 | if-cmcj  | Index  carpometacarpal joint
   *   18 |    7 | if-mcpj  | Index  metacarpal joint
   *   17 |    8 | if-pipj  | Index  proximal interphalangeal joint
   *   16 |    9 | if-dipj  | Index  distal interphalangeal joint
   *   15 |   10 | if-tip   | Index  tip
   *   14 |   11 | mf-cmcj  | Middle carpometacarpal joint
   *   13 |   12 | mf-mcpj  | Middle metacarpal joint
   *   12 |   13 | mf-pipj  | Middle proximal interphalangeal joint
   *   11 |   14 | mf-dipj  | Middle distal interphalangeal joint
   *   11 |   15 | mf-tip   | Middle tip
   *    9 |   16 | rf-cmcj  | Ring   carpometacarpal joint
   *    8 |   17 | rf-mcpj  | Ring   metacarpal joint
   *    7 |   18 | rf-pipj  | Ring   proximal interphalangeal joint
   *    6 |   19 | rf-dipj  | Ring   distal interphalangeal joint
   *    5 |   20 | rf-tip   | Ring   tip
   *    4 |   21 | lf-cmcj  | Little carpometacarpal joint
   *    3 |   22 | lf-mcpj  | Little metacarpal joint
   *    2 |   23 | lf-pipj  | Little proximal interphalangeal joint
   *    1 |   24 | lf-dipj  | Little distal interphalangeal joint
   *    0 |   25 | lf-tip   | Little tip
   */

  /* bones
   *   1 | root   | root / wrist
   *   2 | th-c   | Thumb  carpal
   *   3 | th-mc  | Thumb  metacarpal
   *   4 | th-pp  | Thumb  proximal phalanx
   *   5 | th-dp  | Thumb  distal phalanx
   *   6 | if-c   | Index  carpal
   *   7 | if-mc  | Index  metacarpal
   *   8 | if-pp  | Index  proximal phalanx
   *   9 | if-mp  | Index  middle phalanx
   *  10 | if-dp  | Index  distal phalanx
   *  11 | mf-c   | Middle carpal
   *  12 | mf-mc  | Middle metacarpal
   *  13 | mf-pp  | Middle proximal phalanx
   *  14 | mf-mp  | Middle middle phalanx
   *  15 | mf-dp  | Middle distal phalanx
   *  16 | rf-c   | Ring   carpal
   *  17 | rf-mc  | Ring   metacarpal
   *  18 | rf-pp  | Ring   proximal phalanx
   *  19 | rf-mp  | Ring   middle phalanx
   *  20 | rf-dp  | Ring   distal phalanx
   *  21 | lf-c   | Little carpal
   *  22 | lf-mc  | Little metacarpal
   *  23 | lf-pp  | Little proximal phalanx
   *  24 | lf-mp  | Little middle phalanx
   *  25 | lf-dp  | Little distal phalanx
   */

} // endof saveClickedPoints
