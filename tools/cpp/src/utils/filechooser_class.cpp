
#include <iostream>    /* for cout */
#include <algorithm>   /* for remove and std::sort *//* otherwise it will take a "remove" from cstdio */
#include <cstdio>      /* for popen */
#ifdef _WIN32
#include "dirent.h"    /*  */
#include <sys/stat.h> /* for stat*/
#else
#include <dirent.h>    /*  */
#endif

#include "filechooser_class.h"


/*#define KSDEBUG /**/


/////////////////////////////// PUBLIC ///////////////////////////////////////

//============================= LIFECYCLE ====================================

//-----------------------------------------------------------------------------
FileChooser::FileChooser ( void ) {

#ifdef KSDEBUG
  std::cout << ">>in  ::FileChooser( )" << std::endl;
#endif

  defaultdirectory = "$HOME/";

#ifdef KSDEBUG
  std::cout << ">>out ::FileChooser( )" << std::endl;
#endif

} // endof constructor FileChooser::FileChooser


//-----------------------------------------------------------------------------
/*virtual*/
FileChooser::~FileChooser ( void ) {

} // endof destructor FileChooser::~FileChooser



//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATIONS ===================================

//-----------------------------------------------------------------------------
std::string
FileChooser::chooseFile( std::string filetype /*=""*/ ) {

  std::string command = "zenity --file-selection --title=\"zenity :: Choose a file\"  --filename=";
  command += defaultdirectory;

  if ( !filetype.empty() ) {
    command += " --file-filter=*"+filetype;
  }

  executeCommand ( command );

  return getFile();

} // endof function FileChooser::chooseFile


std::string
FileChooser::chooseDirectory( void ) {

  std::string command = "zenity --file-selection --title=\"zenity :: Choose a directory\"  --directory --filename=";
  command += defaultdirectory;

  executeCommand ( command );

  if ( !chosenfilename.empty() ) {
    chosenfilename += "/";
  }

  return getFile();

} // endof function FileChooser::chooseFile

//-----------------------------------------------------------------------------
std::string
FileChooser::chooseFileByExtension ( std::string extension ) {

  return chooseFile( extension );

} // endof function FileChooser::chooseFileByExtension


//-----------------------------------------------------------------------------
std::string
FileChooser::saveFile( void ) {

//  FILE* ptr_stream;
  std::string command = "zenity --file-selection --title=\"zenity :: Save the file\" --save";
//  std::string intermediateresult;
//  int status = -1;

  executeCommand ( command );

  return getFile();

} // endof function FileChooser::saveFile


//-----------------------------------------------------------------------------
std::vector< std::string>
FileChooser::readDirectory ( std::string directory ) {

  std::string file;
  std::vector < std::string > filelist;

  DIR* ptr_dirstream;
  struct dirent* ptr_direntry;

  ptr_dirstream = opendir( directory.c_str() );
  if ( !ptr_dirstream ) {
    std::cout << "error opening directory, file list is empty" << std::endl;
    return filelist;
  }


  ptr_direntry=readdir( ptr_dirstream );

  while ( ptr_direntry != NULL ) {

    // if entry is a file
    bool is_a_file = false;
#ifdef _WIN32
    //https://stackoverflow.com/questions/2197918/cross-platform-way-of-testing-whether-a-file-is-a-directory
    struct stat s;
    stat( ptr_direntry->d_name, &s );
    is_a_file = ( s.st_mode & S_IFREG );
#else
    is_a_file =( ptr_direntry->d_type == DT_REG );
#endif
      if ( is_a_file ) {

      file = std::string(ptr_direntry->d_name);

      if ( file[0] == '.' ) { /* hidden files */
      } else if ( file[file.length()-1] == '~' ) { /* backup file */
      } else { /* "good" file */
        filelist.push_back( file );
      } // fi use only files (no backup files and no hidden files)

    } // fi entry is a file

    ptr_direntry = readdir( ptr_dirstream );

  } // endwhile not empty directory entry

  closedir( ptr_dirstream );


  std::sort( filelist.begin(), filelist.end() );

  return filelist;

} // endof function FileChooser::readDirectory




//============================= ACCESS      ===================================
//-----------------------------------------------------------------------------
std::string
FileChooser::getFile ( void ) {

  if ( !chosenfilename.empty() ) {
    return chosenfilename;
  } else {
    std::cerr << "File choosing failed, returning an empty string." << std::endl;
    return std::string("");
  }

} // endof function FileChooser::getFilename

std::string
FileChooser::setDefaultDirectory ( std::string defaultdir /* = "" */ ) {

  FILE* ptr_stream;
  std::string command = "zenity --file-selection --title=\"zenity :: Choose a default directory\" --directory --filename=";
  command += defaultdirectory;
  std::string intermediateresult;
  int status = -1;

  if ( defaultdir.empty() ) {
#ifdef _WIN32
    ptr_stream = _popen( static_cast<const char*> ( command.c_str() ), "r" );
#else
    ptr_stream = popen( static_cast<const char*> ( command.c_str() ), "r" );
#endif

    if ( ptr_stream != NULL ) {

      int bytesread;
      const int buffersize = 1024;
      char charbuffer[buffersize + 1];

      while ( ( bytesread = fread ( charbuffer, 1, buffersize, ptr_stream ) ) > 0 ) {
        charbuffer[bytesread] = 0;
        intermediateresult += std::string ( charbuffer );
      }

#ifdef _WIN32
      status = _pclose( ptr_stream );
#else
      status = pclose( ptr_stream );
#endif

    } // fi ptr_stream != NULL

    if ( status == 0 ) {
      intermediateresult.erase(std::remove(intermediateresult.begin(), intermediateresult.end(), '\n'), intermediateresult.end());
      defaultdirectory = intermediateresult;
      defaultdirectory += "/";
    } else {
      std::cerr << "File choosing failed, directory is set to $HOME/." << std::endl;
      defaultdirectory="$HOME/";
    } // fi status == 0

  } else {
    defaultdirectory=defaultdir;
  }// fi defaultdir empty

  return defaultdirectory;

} // endof function FileChooser::setDefaultDirectory



void
FileChooser::unsetDefaultDirectory ( void ) {

  defaultdirectory = "";

} // endof function FileChooser::unsetDefaultDirectory







/////////////////////////////// PROTECTED  ///////////////////////////////////

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATIONS ===================================
/** - none - **/

//============================= ACCESS      ===================================
/** - none - **/




/////////////////////////////// PRIVATE    ///////////////////////////////////

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATIONS ===================================
/** - none - **/
void
FileChooser::executeCommand ( std::string command ) {


  FILE* ptr_stream;
  int status = -1;
  std::string intermediateresult = "";

#ifdef _WIN32
  ptr_stream = _popen( static_cast<const char*> ( command.c_str() ), "r" );
#else
  ptr_stream = popen( static_cast<const char*> ( command.c_str() ), "r" );
#endif

  if ( ptr_stream != NULL ) {

    int bytesread;
    const int buffersize = 1024;
    char charbuffer[buffersize + 1];

    while ( ( bytesread = fread ( charbuffer, 1, buffersize, ptr_stream ) ) > 0 ) {

      charbuffer[bytesread] = 0;
      intermediateresult += std::string ( charbuffer );

    } // endwhile

#ifdef _WIN32
    status = _pclose( ptr_stream );
#else
    status = pclose( ptr_stream );
#endif

  } // fi ptr_stream not empty

  if ( status == 0 ) {

    intermediateresult.erase(std::remove(intermediateresult.begin(), intermediateresult.end(), '\n'), intermediateresult.end());
    chosenfilename = intermediateresult;

  } else {
    std::cerr << "File choosing failed, file is empty." << std::endl;
    //TODO I have no clue how to say there is no file.
  } // fi status ok


} // endof function FileChooser::doTheWork

//============================= ACCESS      ===================================
/** - none - **/

