/** filestorageocv_class -  A one line description of the class.
 *
 * #include "XX.h" <BR>
 * -llib
 *
 * A longer description.
 *
 * @see something
 *  Created on: Nov 08, 2014
 *      Author: kstoll2m
 */

#ifndef CLASS_KS_FILESTORAGEOCV_H_
#define CLASS_KS_FILESTORAGEOCV_H_

#include <string>

#include <opencv/cv.h>

/*                                                                         //**
 * *** start   CLASS   fILEsTORAGEoCV ******************************************** *
\*                                                                           */




/**************************************************************************//**
 *  \brief bla
 *
 * This is a
 *
 *****************************************************************************/
class FileStorageOcv {

public:
// CLASS VARIABLES

// LIFECYCLE

  /** Standard constructor
   */
  FileStorageOcv ( void );



  /** Destructor
   */
  virtual ~FileStorageOcv ( void );

// OPERATORS
  /** - none - **/

// OPERATIONS

  /**
   */
  cv::Mat loadHomography ( std::string filetoloadfrom );

  /**
   */
  cv::Size loadHomographySize ( std::string filetoloadfrom );

  /**
   */
  double loadHomographyScale ( std::string filetoloadfrom );

  /**
   */
  cv::Mat loadIntrinsicMatrix ( std::string filetoloadfrom );

  /**
   */
  cv::Mat loadIntrinsicDistortion ( std::string filetoloadfrom );

  /**
   */
  cv::Mat loadExtrinsicTranslation ( std::string filetoloadfrom );

  /**
   */
  cv::Mat loadExtrinsicRotation ( std::string filetoloadfrom );

/*
  void saveSomething ( void ) {

      cv::FileStorage   cfs;
      cfs.open("test.xml", cv::FileStorage::WRITE);
      cfs << "camera";
      cfs << "{";
      cfs << "intrinsic";
      cfs << "{";
      cfs << "calibration_matrix" << cv::Mat::eye( 3,2,CV_8U );
      cfs << "distortions"        << cv::Mat::ones( 5,1,CV_8U );
      cfs << "}";
      cfs << "extrinic";
      cfs << "{";
      cfs << "translation_vector" << cv::Mat::ones( 3,1,CV_8U );
      cfs << "rotation_vector"    << cv::Mat::ones( 3,1,CV_8U );
      cfs << "}";
      cfs << "}";
      cfs << "object";
      cfs << "{";
      cfs << "coordinates_3d"     << 0. <<   0. << 0. << 50. <<   0. << 0. << 100. <<   0. << 0.
                                  << 0. <<  50. << 0. << 50. <<  50. << 0. << 100. <<  50. << 0.
                                  << 0. << 100. << 0. << 50. << 100. << 0. << 100. << 100. << 0.;
      cfs << "}";
      cfs << "homography";
      cfs << "{";
      cfs << "homography_matrix"  << cv::Mat::eye( 3,2,CV_8U )*5;
      cfs << "scale_factor"       << 5;
      cfs << "board_size"         << cv::Size(150,100);
      cfs << "}";
      cfs.release();
  };
*/

// ACCESS
  /** - none - **/





protected:
// CLASS VARIABLES
  /** - none - **/

// OPERATORS
  /** - none - **/

// OPERATIONS
  /** - none - **/

// ACCESS
  /** - none - **/





private:
// CLASS VARIABLES
  std::string       filename;   // name of the joint
  cv::FileStorage   ocvfs;
  cv::FileNode      ocvfn;
  bool              isopen;



// OPERATORS
  /** - none - **/

// OPERATIONS
  /** - none - **/

// ACCESS
  /** - none - **/

};


#endif /* CLASS_KS_FILESTORAGEOCV_H_*/


