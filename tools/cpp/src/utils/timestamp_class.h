/** timestamp_class.h -  A one line description of the class.
 *
 * #include "XX.h" <BR>
 * -llib
 *
 * A longer description.
 *
 * @see something
 *  Created on: Oct 15, 2014
 *      Author: kstoll2m
 */

#ifndef CLASS_KS_TIMESTAMP_H_
#define CLASS_KS_TIMESTAMP_H_

#ifdef _WIN32 
#include <ctime> /* chrono includes ctime on linux/* for std::time_t */
#endif
#include <string>
#include <chrono>

/*                                                                         //**
 * *** start   CLASS   tIMEsTAMP ************************************* *
\*                                                                           */




/**************************************************************************//**
 *  \brief bla
 *
 * Creates a time stamp and saves it in a string of the format YYYYMMDDhhmmss.ssssssGMT where
 *   YYYY        4-digit year                                      [ 0, 3]
 *   MM          2-digit month                                     [ 4, 5]
 *   DD          2-digit day of month                              [ 6, 7]
 *   hh          2-digit hour of the day                           [ 8, 9]
 *   mm          2-digit minute of the hour                        [10,11]
 *   ss.ssssss   2-digit seconds + 6-digit microseconds / 1000000  [12,13].[15,20]
 *   GMT         GMT time zone abbreviation                        [21,23]
 *
 *****************************************************************************/
class TimeStamp {

public:
// CLASS VARIABLES

// LIFECYCLE

  /** Standard constructor
   */
  TimeStamp ( );

  /** construct time stamp from chrono time point
   *
   * @param
   */
  TimeStamp ( std::chrono::high_resolution_clock::time_point );

  /** construct time stamp from time_t time point
   *
   * @param
   */
  TimeStamp ( std::time_t );

  /** construct time stamp from input string
   *
   * @param   string containing the time stamp
   * @param   format of the string
   *          currently supported:
   *          empty string ... YYYYMMDDhhmmss.ssssssGMT
   */
  TimeStamp ( std::string, std::string format = "" );

  /** Destructor
   */
  virtual ~TimeStamp ( void );


// OPERATORS
  /** - none - **/

// OPERATIONS

  /**update current time stamp
   */
  void update ( void );




// ACCESS

  /**get time in string format (format hhmmss)
   *
   * @return      string containing time (hhmmss)
   */
  std::string getTimeString ( void );
  
  /**get date in string format (format YYYYMMDD)
   *
   * @return      string containing date (YYYYMMDD)
   */
  std::string getDateString ( void );

  /**get time and date in string format (format YYYYMMDDhhmmss)
   *
   * @return      string containing date and time (YYYYMMDDhhmmss)
   */
  std::string getDateAndTimeString ( void );

  /**get time and date in string format (format YYYYMMDDhhmmss.ssssssGMT)
   *
   * @return      string containing date and time (YYYYMMDDhhmmss.ssssssGMT)
   */
  std::string getCompleteString ( void );

  /**get time and date as a chrono timepoint
   *
   * @return      chrono clock time_point containint the date and time
   */
  std::chrono::high_resolution_clock::time_point getChronoTimePoint ( void );



protected:
// CLASS VARIABLES
  /** - none - **/

// OPERATORS
  /** - none - **/

// OPERATIONS
  /** - none - **/

// ACCESS
  /** - none - **/



private:
// CLASS VARIABLES
  std::chrono::high_resolution_clock::time_point   chronotimevalue;
  std::string                                      datestring;


// OPERATORS
  /** - none - **/

// OPERATIONS

  /** convert a chrono clock to a string
   *
   * @param    high_resolution_clock::time_point the time to convert
   * @return   string containing the clock in the format YYYYMMDDhhmmss.ssssssZZZ where
   *           YYYY      4-digit year
   *           MM        2-digit month
   *           DD        2-digit day of month
   *           hh        2-digit hour of the day
   *           mm        2-digit minute of the hour
   *           ss.ssssss 2-digit seconds + 6-digit microseconds / 1000000
   *           ZZZ       3-letter time zone (e.g. GMT)
   */
  std::string chronoClockToString ( std::chrono::high_resolution_clock::time_point );

  /** convert a chrono clock to a string
   *
   * @param    std::string the time to convert
   * @return   time_point containing the clock in the format YYYYMMDDhhmmss.ssssssZZZ where
   *           YYYY      4-digit year
   *           MM        2-digit month
   *           DD        2-digit day of month
   *           hh        2-digit hour of the day
   *           mm        2-digit minute of the hour
   *           ss.ssssss 2-digit seconds + 6-digit microseconds / 1000000
   *           ZZZ       3-letter time zone (e.g. GMT)
   */
  std::chrono::high_resolution_clock::time_point stringToChronoClock ( std::string, std::string format = "" );


// ACCESS
  /** - none - **/


}; // endof class TimeStampString

#endif /* CLASS_KS_TIMESTAMP_H_ */
