#include <iostream>
#include <cstdlib> /* for mktemp */
#ifdef _WIN32
#include <io.h> /*for mktemp */
#endif

#include "filename_class.h"




/*#define KSDEBUG /**/



/////////////////////////////// PUBLIC ///////////////////////////////////////

//============================= LIFECYCLE ====================================

//-----------------------------------------------------------------------------
FileName::FileName ( void ) {
  
#ifdef KSDEBUG
  std::cout << ">>in  ::FileName )" << std::endl;
#endif

  setDefaultName( );
  
#ifdef KSDEBUG
  std::cout << ">>out ::FileName( )" << std::endl;
#endif
} // endof FileName::FileName



//-----------------------------------------------------------------------------
FileName::FileName ( std::string filename ) {
  
#ifdef KSDEBUG
  std::cout << ">>in  ::FileName( string )" << std::endl;
#endif

  setDefaultName( );
  
  splitFileName( filename );
  
#ifdef KSDEBUG
  std::cout << ">>out ::FileName( string )" << std::endl;
#endif
  
} // endof FileName::FileName


//-----------------------------------------------------------------------------
FileName::FileName ( const char* ptr_filename ) {
  
#ifdef KSDEBUG
  std::cout << ">>in  ::FileName( char* )" << std::endl;
#endif

  setDefaultName( );
  
  std::string filename( ptr_filename );

  splitFileName( filename );

#ifdef KSDEBUG
  std::cout << ">>out ::FileName( char* )" << std::endl;
#endif
  
} // endof FileName::FileName




//-----------------------------------------------------------------------------
/*virtual */
FileName::~FileName ( void ) {

} // endof FileName::~FileName



//============================= OPERATORS ====================================
  /** - none - **/

//============================= OPERATIONS ===================================
  /** - none - **/

//============================= ACCESS      ===================================

//-----------------------------------------------------------------------------
std::string 
FileName::getExtension ( void ) {
  
  return ext;
  
} // endof function FileName::getExtension


//-----------------------------------------------------------------------------
std::string 
FileName::getName ( void ) const {
  
  return name;
  
} // endof function FileName::getName


//-----------------------------------------------------------------------------
std::string 
FileName::getPath ( void ) {
  
  return path;
  
} // endof function FileName::getPath


//-----------------------------------------------------------------------------
std::string 
FileName::getFileName ( void ) {
  
  return (name+"."+ext);
  
} // endof function FileName::getFileName


//-----------------------------------------------------------------------------
std::string
FileName::getCompleteFileName ( void ) {

  return (path+name+"."+ext);

} // endof function FileName::getFileName



//-----------------------------------------------------------------------------
void 
FileName::setExtension ( std::string newext ) {
  
  ext = newext;
  
} // endof function FileName::setExtension


//-----------------------------------------------------------------------------
void 
FileName::setName ( std::string newname ) {
  
  name = newname;
  
} // endof function FileName::setName


//-----------------------------------------------------------------------------
void 
FileName::setPath ( std::string newpath ) {
  
  // make sure the path ends with a '/'
  if ( newpath.back() != '/' ) {
    newpath += '/';
  }
  
  path = newpath;
  
} // endof function FileName::setPath






/////////////////////////////// PROTECTED  ///////////////////////////////////

//============================= OPERATORS ====================================
  /** - none - **/

//============================= OPERATORS ====================================
  /** - none - **/

//============================= OPERATIONS ===================================
  /** - none - **/

//============================= ACCESS      ===================================
  /** - none - **/



/////////////////////////////// PRIVATE    ///////////////////////////////////

//============================= OPERATORS ====================================
  /** - none - **/

//============================= OPERATORS ====================================
  /** - none - **/

//============================= OPERATIONS ===================================

//-----------------------------------------------------------------------------
void
FileName::splitFileName ( std::string completefilename ) {
  
#ifdef KSDEBUG
  std::cout << ">>in  ::splitFileName( string ) " << std::endl;
#endif

  int slash;
  int dot;
  // split path from filename
#ifdef _WIN32
  slash = completefilename.find_last_of( "\\" );
#else
  slash = completefilename.find_last_of( "/" );
#endif
  if ( slash < 0 ) {
    // no path in filename present => use default;
  } else {
    path = completefilename.substr(0,slash+1);
  }
  if ( slash+2 > completefilename.length() ) {
    // no name of extension in filname, only path present.
    return;
  }

  // remove path from filename
  completefilename = completefilename.substr(slash+1);
  
  // split extension from name
  // from here on, no slash is present in the filename.
  dot = completefilename.find_last_of( "." );
  if ( dot < 0 ) {
    // no dot present in filename => use default extension;
    name = completefilename;
  } else {
    ext = completefilename.substr( dot+1 );
    name = completefilename.substr( 0,dot );
  }

#ifdef KSDEBUG
  std::cout << ">>out ::splitFileName( string ) " << std::endl;
#endif

}


//============================= ACCESS      ===================================

//-----------------------------------------------------------------------------
void 
FileName::setDefaultName ( void ) {

#ifdef KSDEBUG
  std::cout << ">>in  ::setDefaultName( ) " << std::endl;
#endif

  // create 
  char filepattern[] = "temp-ks_XXXXXX";
#ifdef _WIN32
  _mktemp_s(filepattern);
#else
  mktemp(filepattern);
#endif
  path = "./";          // this directory
  name = filepattern;   // some random filename
  ext  = "txt";         // just a textfile

#ifdef KSDEBUG
  std::cout << ">>out ::setDefaultName( ) " << std::endl;
#endif

} // endof function FileName::setDefaultName




