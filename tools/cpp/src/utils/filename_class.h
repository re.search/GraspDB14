/** filename_class -  A one line description of the class.
 *
 * #include "XX.h" <BR>
 * -llib
 *
 * A longer description.
 *
 * @see something
 *  Created on: Oct 14, 2014
 *      Author: kstoll2m
 */

#ifndef CLASS_KS_FILENAME_H_
#define CLASS_KS_FILENAME_H_

#include <string>

/*                                                                         //**
 * *** start   CLASS   fILEnAME ******************************************** *
\*                                                                           */




/**************************************************************************//**
 *  \brief bla
 *
 * This is a 
 *
 *****************************************************************************/
class FileName {

public:
// CLASS VARIABLES

// LIFECYCLE

  /** Standard empty constructor
   */
  FileName ( void );

  /** Standard constructor from a string
   *
   * @param string containing the file name to generate
   */
  FileName ( std::string filename );

  /** Standard constructor from a char
   *
   * @param char array containing the file name to generate
   */
  FileName ( const char* ptr_filename );

  /** Destructor
   */
  virtual ~FileName ( void );




// OPERATORS
  /** - none - **/

// OPERATIONS
  /** - none - **/

// ACCESS

  /** get the extension of a file
   *
   * @return   string containing the extension of the file
   */
  std::string getExtension ( void );
  
  
  /** get the name of a file
   *
   * @return   string containing the name of the file
   */
  std::string getName ( void ) const;


  /** get the path of a file
   *
   * @return   string containing the path of the file
   */
  std::string getPath ( void );

  /** get the name of a file with the extension
   *
   * @return   string containing the file name
   */
  std::string getFileName ( void );
  
  /** get the complete name of a file with the extension and path
   *
   * @return   string containing the complete file name
   */
  std::string getCompleteFileName ( void );

  
  
  
  /** set the extension of a file
   *
   * @param   string containing the extension
   */
  void setExtension ( std::string );


  /** set the name of a file
   *
   * @param   string containing the name
   */
  void setName ( std::string );


  /** set the path of a file
   *
   * @param   string containing the path
   */
  void setPath ( std::string );




protected:
// CLASS VARIABLES
  /** - none - **/

// OPERATORS
  /** - none - **/

// OPERATIONS
  /** - none - **/

// ACCESS
  /** - none - **/



private:
// CLASS VARIABLES
std::string path;
std::string name;
std::string ext;

// OPERATORS
  /** - none - **/

// OPERATIONS

  /** set default filename
   *
   * @param   string containing the original filename and path
   */
  void splitFileName ( std::string );


// ACCESS

  /** set default filename
   */
  void setDefaultName ( void );



}; // endof class classname

#endif /* CLASS_KS_FILENAME_H_ */
