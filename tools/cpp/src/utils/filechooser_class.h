/** filechooser_class - opens a dialog for choosing a file.
 *
 * #include "XX.h" <BR>
 * -llib
 *
 *  needs zenity
 *
 * This class opens a simple dialog for choosing a file and saves the chosen filename as string.
 *
 * @see something
 *  Created on: Nov 04, 2014
 *      Author: kstoll2m
 */

#ifndef CLASS_KS_FILECHOOSER_H_
#define CLASS_KS_FILECHOOSER_H_

#include <string>
#include <vector>


/*                                                                         //**
 * *** start   CLASS   fILEcHOOSER ***************************************** *
\*                                                                           */




/**************************************************************************//**
 *  \brief Opens a dialog (zenity only) for choosing a file
 *
 * This class opens a simple dialog (zenity only) for choosing a file
 * and saves the chosen filename as string
 *
 *****************************************************************************/

class FileChooser {

  // partly inspired by http://www.juce.com/forum/topic/filechooser-multi-selection-and-zenity-platform-dialog
  // see further down (at the end of the file)

public:
// CLASS VARIABLES

// LIFECYCLE

    /** Standard constructor
     */
  FileChooser ( void );

  /** Destructor
   */
  virtual ~FileChooser ( void );



// OPERATORS
  /** - none - **/

// OPERATIONS
  /**
   */
  std::string chooseFile ( std::string = "" );

  /**
   */
  std::string chooseDirectory ( void );

  /**
   */
  std::string chooseFileByExtension ( std::string = "" );

  /**
   */
  std::string saveFile ( void );

  /**
   */
  std::vector< std::string> readDirectory ( std::string );



// ACCESS
  /**
   */
  std::string getFile ( void );

  /**
   */
  std::string setDefaultDirectory ( std::string = "" );

  /**
   */
  void unsetDefaultDirectory ( void );




protected:
// CLASS VARIABLES
  /** - none - **/

// OPERATORS
  /** - none - **/

// OPERATIONS
  /** - none - **/

// ACCESS
  /** - none - **/





private:
// CLASS VARIABLES
  std::string chosenfilename;
  std::string defaultdirectory;

// OPERATORS
  /** - none - **/

// OPERATIONS
  /** - none - **/
  void executeCommand ( std::string command );

// ACCESS
  /** - none - **/

};

#endif /* CLASS_KS_FILECHOOSER_H_*/

/*
//==============================================================================
void FileChooser::showPlatformDialog (OwnedArray<File>& results,
                                      const String& title,
                                      const File& file,
                                      const String& filters,
                                      bool isDirectory,
                                      bool isSave,
                                      bool warnAboutOverwritingExistingFiles,
                                      bool selectMultipleFiles,
                                      FilePreviewComponent* previewComponent)
{
    String separator = "|";
    String command = "zenit --file-selection";
    String resultString;

    if (title != String::empty)
        command << " --title=\"" << title << "\"";

    if (file != File::nonexistent)
        command << " --filename=\"" << file.getFullPathName () << "\"";

    if (isDirectory)
        command << " --directory";

    if (isSave)
        command << " --save";

    if (selectMultipleFiles)
        command << " --multiple --separator=\"" << separator << "\"";

    command << " 2>&1";

    int status = -1;
    FILE* stream = popen ((const char*) command, "r");
    if (stream != NULL)
    {
        int bytesRead, bufferSize = 1024;
        char buffer[bufferSize + 1];

        while ((bytesRead = fread (buffer, 1, bufferSize, stream)) > 0)
        {
            buffer[bytesRead] = 0;
            resultString += String (buffer);
        }

        status = pclose (stream);
    }

    if (status == 0)
    {
        StringArray tokens;

        if (selectMultipleFiles)
            tokens.addTokens (resultString, separator, 0);
        else
            tokens.add (resultString);

        for (int i = 0; i < tokens.size(); i++)
            results.add (new File (tokens[i]));

        return;
    }

    //xxx ain't got one!
    jassertfalse
}
*/
