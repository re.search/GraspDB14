
#include <iostream>

#include "filestoragexml_class.h"



/*#define KSDEBUG /**/

/////////////////////////////// PUBLIC ///////////////////////////////////////

//============================= LIFECYCLE ====================================

//-----------------------------------------------------------------------------
FileStorageXml::FileStorageXml ( void ) {

#ifdef KSDEBUG
  std::cout << ">>in  ::FileStorageXml( )" << std::endl;
#endif

  isopen = false;

#ifdef KSDEBUG
  std::cout << ">>out ::FileStorageXml( )" << std::endl;
#endif

} // endof constructor

//-----------------------------------------------------------------------------
FileStorageXml::FileStorageXml ( std::string filetoloadfrom ) {

#ifdef KSDEBUG
  std::cout << ">>in  ::FileStorageXml( std::string )" << std::endl;
#endif

  filename = filetoloadfrom;
  isopen = false;

#ifdef KSDEBUG
  std::cout << ">>out ::FileStorageXml( std::string )" << std::endl;
#endif

} // endof constructor

//-----------------------------------------------------------------------------
/*virtual*/
FileStorageXml::~FileStorageXml ( void ) {

} // endof destructor



//============================= OPERATORS ====================================
  /** - none - **/

//============================= OPERATIONS ===================================

//-----------------------------------------------------------------------------
Eigen::Matrix3d
FileStorageXml::loadHomography ( std::string filetoloadfrom /*=""*/ ) {

  std::cerr << "  FileStorageXml::loadHomography not yet implemented, returning identity matrix" << std::endl;

  return Eigen::Matrix3d::Identity();

} // funtion FileStorageOcv::loadHomography



//-----------------------------------------------------------------------------
//FileStorageXml::loadHomographySize ( void ) {} // funtion FileStorageXml::loadHomographySize



//-----------------------------------------------------------------------------
double
FileStorageXml::loadHomographyScale ( std::string filetoloadfrom /*=""*/ ) {




  // double string extraction related
  std::string nodetext;
  double scalefactor = 0;

  std::string nodesuccession = "";

  // tinyxml related
  tinyxml2::XMLElement* ptr_elem;
  tinyxml2::XMLError xmlerr;



  if ( !filetoloadfrom.empty() ) filename = filetoloadfrom;
  xmlerr = xmldoc.LoadFile( filename.c_str() );
  if ( xmlerr ) {
    xmldoc.PrintError();
    return scalefactor;
  }


  nodesuccession += "homography";
  nodesuccession += " scale_factor";


  ptr_elem = navigateTo( nodesuccession );

  if ( !ptr_elem ) {
    return scalefactor;
  };





  //
  // get scalefactor and return it
  //
  nodetext.assign( ptr_elem->GetText() );
  scalefactor = std::stod( nodetext );


  return scalefactor;

} // funtion FileStorageXml::loadHomographyScale



//-----------------------------------------------------------------------------
Eigen::Matrix3d
FileStorageXml::loadIntrinsicMatrix ( std::string filetoloadfrom /*=""*/ ){

  std::cerr << "  FileStorageXml::loadIntrinsicMatrix not yet implemented, returning identity matrix" << std::endl;

  return Eigen::Matrix3d::Identity();

} // endof function FileStorageXml::loadIntrinsicMatrix



//-----------------------------------------------------------------------------
Eigen::Vector8d
FileStorageXml::loadIntrinsicDistortion ( std::string filetoloadfrom /*=""*/ ){

  std::cerr << "  FileStorageXml::loadIntrinsicDistortion not yet implemented, returning zero vector" << std::endl;

  return Eigen::Vector8d::Zero();


} // endof function FileStorageXml::loadIntrinsicDistortion



//-----------------------------------------------------------------------------
Eigen::Vector3d
FileStorageXml::loadExtrinsicTranslation ( std::string filetoloadfrom /*=""*/ ){

  std::cerr << "  FileStorageXml::loadExtrinsicTranslation not yet implemented, returning zero vector" << std::endl;

  return Eigen::Vector3d::Zero();

} // endof function FileStorageXml::loadExtrinsicTranslation



//-----------------------------------------------------------------------------
Eigen::Vector3d
FileStorageXml::loadExtrinsicRotation ( std::string filetoloadfrom /*=""*/ ) {

  std::cerr << "  FileStorageXml::loadExtrinsicRotation not yet implemented, returning zero vector" << std::endl;

  return Eigen::Vector3d::Zero();

} // endof function FileStorageXml::loadExtrinsicRotation



//-----------------------------------------------------------------------------
void
FileStorageXml::loadHand2dCoordinates( std::vector< double >& ref_coordinates2dout,
                                       std::string            filetoloadfrom     /* ="" */,
                                       unsigned int           flags              /* =0  */ ) {

  // double string extraction related
  std::string nodetext;
  std::stringstream coordinatestream;
  double coordinate;

  std::string nodesuccession = "";

  // tinyxml related
  tinyxml2::XMLElement* ptr_elem;
  tinyxml2::XMLError xmlerr;

  ref_coordinates2dout.clear();



  if ( !filetoloadfrom.empty() ) filename = filetoloadfrom;
  xmlerr = xmldoc.LoadFile( filename.c_str() );
  if ( xmlerr ) {
    xmldoc.PrintError();
    return;
  }


  nodesuccession += "hand";

  // get left or right hand and check if element was present in file
  if ( flags & LEFT ) nodesuccession += " left"; // todo
  else nodesuccession += " right";

  // get image based measured or ??? data in left or right hand and check if element was present in file
  if ( flags & FROMIMG ) nodesuccession += " image_based_measurement";
  else nodesuccession += " measurement"; // todo

  // get raw or scaled data in left or right hand and check if element was present in file
  if ( flags & RAW) nodesuccession += " raw";
  else nodesuccession += " scaled";

  // get final data element check if element was present in file
  nodesuccession += " coordinates_2d";




  ptr_elem = navigateTo( nodesuccession );

  if ( !ptr_elem ) {
    return;
  };





  //
  // get coordinates and save them in the output vector
  //
  nodetext.assign( ptr_elem->GetText() );
  coordinatestream << nodetext;

  coordinate = 0;
  while ( coordinatestream >> coordinate ) {
    ref_coordinates2dout.push_back( coordinate );
//    std::cout << coordinate << ' ';
  } // endwhile

//  std::cout << "success: " << ptr_elem->GetText() << std::endl;

} // endof function FileStorageXml::loadHand2dCoordinates




//-----------------------------------------------------------------------------
void
FileStorageXml::loadHandLengths( std::vector< double >& ref_lengthsout,
                                 std::string            filetoloadfrom /* ="" */,
                                 unsigned int           flags          /* =0 */ ) {

  // double string extraction related
  std::string nodetext;
  std::stringstream lengthstream;
  std::string nodesuccession = "";
  double length;

  // tinyxml related
  tinyxml2::XMLElement* ptr_elem;
  tinyxml2::XMLError xmlerr;

  ref_lengthsout.clear();



  if ( !filetoloadfrom.empty() ) filename = filetoloadfrom;
  xmlerr = xmldoc.LoadFile( filename.c_str() );
  if ( xmlerr ) {
    xmldoc.PrintError();
    return;
  }



  nodesuccession += "hand";

  // get left or right hand and check if element was present in file
  if ( flags & LEFT ) nodesuccession += " left"; // todo
  else nodesuccession += " right";

  // get image based measured or ??? data in left or right hand and check if element was present in file
  if ( flags & FROMIMG ) nodesuccession += " image_based_measurement";
  else nodesuccession += " measurement"; // todo

  // get raw or scaled data in left or right hand and check if element was present in file
  if ( flags & RAW) nodesuccession += " raw";
  else nodesuccession += " scaled";

  // get final data element check if element was present in file
  nodesuccession += " lengths";



  ptr_elem = navigateTo( nodesuccession );

  if ( !ptr_elem ) {
    return;
  };


  //
  // get coordinates and save them in the output vector
  //
  nodetext.assign( ptr_elem->GetText() );
  lengthstream << nodetext;

  length = 0;
  while ( lengthstream >> length ) {
    ref_lengthsout.push_back( length );
//    std::cout << length << ' ';
  } // endwhile

//  std::cout << "success: " << ptr_elem->GetText() << std::endl;


} // endof function FileStorageXml::loadHandLengths




//============================= ACCESS      ===================================
/** - none - **/








/////////////////////////////// PROTECTED  ///////////////////////////////////

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATIONS ===================================
/** - none - **/

//============================= ACCESS      ===================================
/** - none - **/




/////////////////////////////// PRIVATE    ///////////////////////////////////

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATIONS ===================================
tinyxml2::XMLElement*
FileStorageXml::navigateTo ( std::string elementsuccession ) {

  tinyxml2::XMLElement* ptr_element;
  std::stringstream elementstream;
  std::string token;

  elementstream << elementsuccession;

  // get first element
  elementstream >> token;
  ptr_element = xmldoc.FirstChildElement();
  xmldoc.RootElement();
  if (std::string(ptr_element->Value()) == "opencv_storage") ptr_element = ptr_element->FirstChildElement( token.c_str() );
  else ptr_element = xmldoc.FirstChildElement( token.c_str() );
  // and check if valid
  if ( !ptr_element ) {
    std::cerr << "  something went wrong ... there's no <" << token << ">." << std::endl;
    return ptr_element;
  }



  // go through elements
  while ( elementstream >> token ) {

    ptr_element = ptr_element->FirstChildElement( token.c_str() );
    if ( !ptr_element ) {
      std::cerr << "  something went wrong ... there's no <" << token << ">. " << std::endl;
      return ptr_element;
    }
  }

  std::cout << "  returning node <" << token << ">" << std::endl;

  return  ptr_element;

}
//============================= ACCESS      ===================================
/** - none - **/

