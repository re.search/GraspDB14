
#include <iostream>

#include "filestorageocv_class.h"



/*#define KSDEBUG /**/

/////////////////////////////// PUBLIC ///////////////////////////////////////

//============================= LIFECYCLE ====================================

//-----------------------------------------------------------------------------
FileStorageOcv::FileStorageOcv ( void ) {

#ifdef KSDEBUG
  std::cout << ">>in  ::FileStorageOcv( )" << std::endl;
#endif

  isopen = false;

#ifdef KSDEBUG
  std::cout << ">>out ::FileStorageOcv( )" << std::endl;
#endif

} // endof constructor


//-----------------------------------------------------------------------------
/*virtual*/
FileStorageOcv::~FileStorageOcv ( void ) {

} // endof destructor



//============================= OPERATORS ====================================
  /** - none - **/

//============================= OPERATIONS ===================================

//-----------------------------------------------------------------------------
cv::Mat
FileStorageOcv::loadHomography ( std::string filetoloadfrom ) {

  cv::Mat   matrix;

  isopen = ocvfs.open( filetoloadfrom, cv::FileStorage::READ);
  ocvfn = ocvfs["homography"];
  ocvfn["homography_matrix"] >> matrix;
  ocvfn["scale_factor"];
  ocvfn["board_size"];
  ocvfs.release();
  isopen = false;

  if ( matrix.empty() )
    std::cerr << "::loadHomography\n  " << "empty matrix: " << matrix << std::endl;

  return matrix;

} // funtion FileStorageOcv::loadHomography



//-----------------------------------------------------------------------------
cv::Size
FileStorageOcv::loadHomographySize ( std::string filetoloadfrom ) {

  cv::Size   imagesize;

  isopen = ocvfs.open( filetoloadfrom, cv::FileStorage::READ);
  ocvfn = ocvfs["homography"];
  ocvfn["board_size"] >> imagesize;
  ocvfs.release();
  isopen = false;

  if ( imagesize.height==0 || imagesize.width==0 )
    std::cout << "::loadHomographySize\n  " << "empty size: " << imagesize << std::endl;

  return imagesize;

} // funtion FileStorageOcv::loadHomographySize



//-----------------------------------------------------------------------------
double
FileStorageOcv::loadHomographyScale ( std::string filetoloadfrom ) {

  double   scalefactor;

  isopen = ocvfs.open(filetoloadfrom, cv::FileStorage::READ);
  ocvfn = ocvfs["homography"];
  ocvfn["scale_factor"] >> scalefactor;
  ocvfs.release();
  isopen = false;

  if ( scalefactor == 0 )
    std::cout << "::loadHomographyScale\n  " << "scale factor: " << scalefactor << std::endl;
  //else std::cout << "loaded scale factor: " << scalefactor << std::endl;

  return scalefactor;

} // funtion FileStorageOcv::loadHomographyScale



//-----------------------------------------------------------------------------
cv::Mat
FileStorageOcv::loadIntrinsicMatrix ( std::string filetoloadfrom ){

  cv::Mat   matrix;

  isopen = ocvfs.open( filetoloadfrom, cv::FileStorage::READ);
  ocvfn = ocvfs["camera"]["intrinsic"];
  ocvfn["calibration_matrix"] >> matrix;
  ocvfs.release();
  isopen = false;

  if ( matrix.empty() )
    std::cerr << "::loadIntrinsicMatrix\n  " << "empty matrix: " << matrix << std::endl;

  return matrix;

} // endof function FileStorageOcv::loadIntrinsicMatrix



//-----------------------------------------------------------------------------
cv::Mat
FileStorageOcv::loadIntrinsicDistortion ( std::string filetoloadfrom ){

  cv::Mat   matrix;

  isopen = ocvfs.open( filetoloadfrom, cv::FileStorage::READ);
  ocvfn = ocvfs["camera"]["intrinsic"];
  ocvfn["distortions"] >> matrix;
  ocvfs.release();
  isopen = false;

  if ( matrix.empty() )
    std::cerr << "::loadIntrinsicDistortion\n  " << "empty matrix: " << matrix << std::endl;

  return matrix;

} // endof function FileStorageOcv::loadIntrinsicDistortion



//-----------------------------------------------------------------------------
cv::Mat
FileStorageOcv::loadExtrinsicTranslation ( std::string filetoloadfrom ){

  cv::Mat   matrix;

  isopen = ocvfs.open( filetoloadfrom, cv::FileStorage::READ);
  ocvfn = ocvfs["camera"]["extrinsic"];
  ocvfn["translation_vector"] >> matrix;
  ocvfs.release();
  isopen = false;

  if ( matrix.empty() )
    std::cerr << "::loadExtrinsicTranslation\n  " << "empty matrix: " << matrix << std::endl;

  return matrix;

} // endof function FileStorageOcv::loadExtrinsicTranslation



//-----------------------------------------------------------------------------
cv::Mat
FileStorageOcv::loadExtrinsicRotation ( std::string filetoloadfrom ) {

  cv::Mat   matrix;

  isopen = ocvfs.open( filetoloadfrom, cv::FileStorage::READ );
  ocvfn = ocvfs["camera"]["extrinsic"];
  ocvfn["rotation_vector"] >> matrix;
  ocvfs.release();
  isopen = false;

  if ( matrix.empty() )
    std::cerr << "::loadExtrinsicRotation\n  " << "empty matrix: " << matrix << std::endl;

  return matrix;

} // endof function FileStorageOcv::loadExtrinsicRotation




//============================= ACCESS      ===================================
/** - none - **/








/////////////////////////////// PROTECTED  ///////////////////////////////////

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATIONS ===================================
/** - none - **/

//============================= ACCESS      ===================================
/** - none - **/




/////////////////////////////// PRIVATE    ///////////////////////////////////

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATIONS ===================================
/** - none - **/

//============================= ACCESS      ===================================
/** - none - **/

