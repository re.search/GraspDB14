/** filestorageocv_class -  A one line description of the class.
 *
 * #include "XX.h" <BR>
 * -llib
 *
 * A longer description.
 *
 * @see something
 *  Created on: Nov 08, 2014
 *      Author: kstoll2m
 */

#ifndef CLASS_KS_FILESTORAGEXML_H_
#define CLASS_KS_FILESTORAGEXML_H_

#include <string>
#include <vector>

#include <tinyxml2.h>
#include <Eigen/Core>


#ifndef EIGENVECTOR8D
#define EIGENVECTOR8D
namespace Eigen {
  typedef Matrix<double, 8, 1> Vector8d;
}
#endif


enum {
  LEFT    = 0x00000001,   // left hand (if not set => right  hand)
  RAW     = 0x00000010,   // raw data (if not set => scaled data)
  FROMIMG = 0x00000100    // data retrieved from image
};

/*                                                                         //**
 * *** start   CLASS   fILEsTORAGEoCV ******************************************** *
\*                                                                           */




/**************************************************************************//**
 *  \brief bla
 *
 * This is a
 *
 *****************************************************************************/
class FileStorageXml {

public:
// CLASS VARIABLES

// LIFECYCLE

  /** Standard constructor
   */
  FileStorageXml ( void );

  /** Standard constructor
   */
  FileStorageXml ( std::string );


  /** Destructor
   */
  virtual ~FileStorageXml ( void );

// OPERATORS
  /** - none - **/

// OPERATIONS

  /**
   */
  Eigen::Matrix3d loadHomography ( std::string="" );

  /**
   */
  //cv::Size loadHomographySize ( std::string filetoloadfrom );

  /**
   */
  double loadHomographyScale ( std::string="" );

  /**
   */
  Eigen::Matrix3d loadIntrinsicMatrix ( std::string="" );

  /**
   */
  Eigen::Vector8d loadIntrinsicDistortion ( std::string="" );

  /**
   */
  Eigen::Vector3d loadExtrinsicTranslation ( std::string="" );

  /**
   */
  Eigen::Vector3d loadExtrinsicRotation ( std::string="" );

  /**
   */
  void loadHand2dCoordinates( std::vector< double >&, std::string="", unsigned int=0 );

  /**
   */
  void loadHandLengths( std::vector< double >&, std::string="", unsigned int=0 );


// ACCESS
  /** - none - **/





protected:
// CLASS VARIABLES
  /** - none - **/

// OPERATORS
  /** - none - **/

// OPERATIONS
  /** - none - **/

// ACCESS
  /** - none - **/





private:
// CLASS VARIABLES
  std::string             filename;   // name of the file
  tinyxml2::XMLDocument   xmldoc;
  //cv::FileNode            ocvfn;
  bool                    isopen;



// OPERATORS
  /** - none - **/

// OPERATIONS
  tinyxml2::XMLElement* navigateTo ( std::string );

// ACCESS
  /** - none - **/

};


#endif /* CLASS_KS_FILESTORAGEOCV_H_*/


