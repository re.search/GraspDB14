#include <sstream>    /* std::stringstream */
#include <iostream>   /* std::cout */
/*#include <iomanip>    /* std::setw */

#include "timestamp_class.h"

/*#define KSDEBUG /**/

namespace std {
  namespace chrono {
    typedef duration<int, std::ratio_multiply<std::chrono::hours::period, std::ratio<24> >::type> days;
  }
}


/////////////////////////////// PUBLIC ///////////////////////////////////////

//============================= LIFECYCLE ====================================

//-----------------------------------------------------------------------------
TimeStamp::TimeStamp ( ) {

#ifdef KSDEBUG
  std::cout << ">>in  ::TimeStamp( ) " << std::endl;
#endif

  chronotimevalue = std::chrono::high_resolution_clock::now();
  datestring = chronoClockToString( chronotimevalue );
  // YYYYMMDDhhmmss.ssssssGMT

#ifdef KSDEBUG
  std::cout << "  " << datestring << std::endl;
  std::cout << ">>out ::TimeStamp( ) " << std::endl;
#endif


  /*
  std::chrono::microseconds microsecondssinceepoch;
  microsecondssinceepoch = chronotimevalue.time_since_epoch();
  std::cout << ""
      << " " << std::chrono::duration_cast<std::chrono::days>(microsecondssinceepoch).count() << "d"
      << " " << std::chrono::duration_cast<std::chrono::hours>(microsecondssinceepoch).count() % 24
      << ":" << std::chrono::duration_cast<std::chrono::minutes>(microsecondssinceepoch).count() % 60
      << ":" << std::chrono::duration_cast<std::chrono::seconds>(microsecondssinceepoch).count() % 60
      << ":" << microsecondssinceepoch.count() % 1000000
      << "[" << std::chrono::high_resolution_clock::duration::period::num
      << "/" << std::chrono::high_resolution_clock::duration::period::den << "]"
      << std::endl;
  */

} // endof empty constructor




//-----------------------------------------------------------------------------
TimeStamp::TimeStamp ( std::chrono::high_resolution_clock::time_point chronotimepoint ) {

#ifdef KSDEBUG
  std::cout << ">>in  ::TimeStamp(timepoint) " << std::endl;
#endif

  chronotimevalue = chronotimepoint;
  datestring = chronoClockToString( chronotimevalue );

#ifdef KSDEBUG
  std::cout << "  " << datestring << std::endl;
  std::cout << ">>out ::TimeStamp(timepoint) " << std::endl;
#endif

} // endof constructor from chrono time point




//-----------------------------------------------------------------------------
TimeStamp::TimeStamp ( std::time_t timettimepoint ) {

#ifdef KSDEBUG
  std::cout << ">>in  ::TimeStamp(time_t) " << std::endl;
#endif

  chronotimevalue = std::chrono::high_resolution_clock::from_time_t( timettimepoint );
  datestring = chronoClockToString( chronotimevalue );

#ifdef KSDEBUG
  std::cout << ">>out ::TimeStamp(time_t) " << std::endl;
#endif

} // endof constructor from time_t




//-----------------------------------------------------------------------------
TimeStamp::TimeStamp ( std::string dateandtimestring, std::string format /* = "" */ ) {

#ifdef KSDEBUG
  std::cout << ">>in  ::TimeStamp(string, string) " << std::endl;
#endif

  chronotimevalue = stringToChronoClock ( dateandtimestring, format );
  datestring = chronoClockToString( chronotimevalue );

#ifdef KSDEBUG
  std::cout << "  TimeStamp constructor(string, string) old: " << dateandtimestring << std::endl;
  std::cout << "  TimeStamp constructor(string, string) new: " << datestring << std::endl;
  std::cout << ">>out ::TimeStamp(string, string) " << std::endl;
#endif

} // endof constructor from string



//-----------------------------------------------------------------------------
/*virtual*/
TimeStamp::~TimeStamp ( void ) {

} // endof destructor 




//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATIONS ===================================

//-----------------------------------------------------------------------------
void 
TimeStamp::update ( void ) {

#ifdef KSDEBUG
  std::cout << ">>in  ::update " << std::endl;
#endif

  chronotimevalue = std::chrono::high_resolution_clock::now();
  datestring = chronoClockToString( chronotimevalue );

#ifdef KSDEBUG
  std::cout << ">>out ::update " << std::endl;
#endif

} // endof function TimeStampString::update






//============================= ACCESS      ===================================

/*
 *   YYYY        4-digit year                                      [ 0, 3]
 *   MM          2-digit month                                     [ 4, 5]
 *   DD          2-digit day of month                              [ 6, 7]
 *   hh          2-digit hour of the day                           [ 8, 9]
 *   mm          2-digit minute of the hour                        [10,11]
 *   ss.ssssss   2-digit seconds + 6-digit microseconds / 1000000  [12,13].[15,20]
 *   GMT         GRM time zone abbreviation                        [21,23]
 */

//-----------------------------------------------------------------------------
std::string
TimeStamp::getTimeString ( void ) {

  return datestring.substr(8,13);

} // endof function TimeStamp::getTime


//-----------------------------------------------------------------------------
std::string
TimeStamp::getDateString ( void ) {

  return datestring.substr(0,8);

} // endof function TimeStamp::getDate



//-----------------------------------------------------------------------------
std::string
TimeStamp::getDateAndTimeString ( void ) {

  return datestring.substr(0,14);

} // endof function TimeStamp::getDateAndTime



//-----------------------------------------------------------------------------
std::string
TimeStamp::getCompleteString ( void ) {

  return datestring;

} // endof function TimeStamp::getCompleteString



//-----------------------------------------------------------------------------
std::chrono::high_resolution_clock::time_point
TimeStamp::getChronoTimePoint( void ) {

  return chronotimevalue;

}// endof function TimeStamp::getChronoTimePoint




/////////////////////////////// PROTECTED  ///////////////////////////////////

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATIONS ===================================
/** - none - **/

//============================= ACCESS      ===================================
/** - none - **/



/////////////////////////////// PRIVATE    ///////////////////////////////////

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATORS ====================================
/** - none - **/

//============================= OPERATIONS ===================================

//-----------------------------------------------------------------------------
std::string
TimeStamp::chronoClockToString ( std::chrono::high_resolution_clock::time_point chronoclocktimepoint ) {

#ifdef KSDEBUG
  std::cout << ">>in  ::chronoClockToString(time_point) " << std::endl;
#endif

  char datebuffer[20];
  std::time_t timevalue;
  std::tm timevaluegmt;
  std::chrono::high_resolution_clock::time_point chronobefore;
  std::chrono::microseconds elapsedmicroseconds;
  std::string outputclockstring;

  // set time value
  timevalue = std::chrono::high_resolution_clock::to_time_t( chronoclocktimepoint );

  // convert to tm in order to be able to extract seconds etc.
  timevaluegmt = *std::gmtime( &timevalue );
  // convert back to chrono time (=> looses microsecond part)
  chronobefore = std::chrono::high_resolution_clock::from_time_t(timevalue);

  // and subtract from original time
  elapsedmicroseconds = chronoclocktimepoint-chronobefore;


  // set date and time (YYYYMMDDhhmmss)
  std::strftime ( datebuffer, sizeof(datebuffer), "%Y%m%d%H%M%S", &timevaluegmt );
  outputclockstring = std::string( datebuffer );

  // add microseconds (YYYYMMDDhhmmss.ssssss)
  std::snprintf( datebuffer, sizeof( datebuffer ), "%06d", elapsedmicroseconds.count() );
  std::string ms ( datebuffer );
  outputclockstring += "." + ms;

  // add timezone (e.g. YYYYMMDDhhmmss.ssssssGMT)
  outputclockstring += timevaluegmt.tm_zone;

/*
  std::chrono::duration<double> elapsedseconds;
  elapsedseconds = chronotimevalue-chronobefore;

  std::cout << timevaluegmt.tm_year+1900
      << std::setw(2) << std::setfill('0') << timevaluegmt.tm_mon+1
      << std::setw(2) << std::setfill('0') << timevaluegmt.tm_mday
      << std::setw(2) << std::setfill('0') << timevaluegmt.tm_hour
      << std::setw(2) << std::setfill('0') << timevaluegmt.tm_min
      << std::setw(2) << std::setprecision(12) << std::setfill('0') << timevaluegmt.tm_sec + elapsedseconds.count()
      << timevaluegmt.tm_zone
      << std::endl;
*/
#ifdef KSDEBUG
  std::cout << ">>out ::chronoClockToString(time_point) " << std::endl;
#endif

  return outputclockstring;


} // endof function TimeStamp::chronoClockToString





//-----------------------------------------------------------------------------
std::chrono::high_resolution_clock::time_point
TimeStamp::stringToChronoClock ( std::string stringtimepoint, std::string format /* = "" */) {

#ifdef KSDEBUG
  std::cout << ">>in  ::stringToChronoClock(string, string) " << std::endl;
  std::cout << "  timepoint: " << stringtimepoint << std::endl;
  std::cout << "  format:    " << format << std::endl;
#endif

  std::chrono::high_resolution_clock::time_point chronotimepoint;
  std::time_t timettimepoint;
  std::tm timetmvalue = {0};
  std::chrono::microseconds museconds(0);

  if ( format.size() == 0) {

    // => format = "%Y%m%d%H%M%S"; // = YYYYMMDDhhmmss

    // format YYYYMMDDhhmmss.ssssssZZZ where
    timetmvalue.tm_year = std::stoi( stringtimepoint.substr(  0,4 ) ) - 1900;
    timetmvalue.tm_mon  = std::stoi( stringtimepoint.substr(  4,2 ) ) - 1;
    timetmvalue.tm_mday = std::stoi( stringtimepoint.substr(  6,2 ) );
    timetmvalue.tm_hour = std::stoi( stringtimepoint.substr(  8,2 ) );
    timetmvalue.tm_min  = std::stoi( stringtimepoint.substr( 10,2 ) );
    timetmvalue.tm_sec  = std::stoi( stringtimepoint.substr( 12,2 ) );
    timetmvalue.tm_zone = stringtimepoint.substr( 21 ).c_str();
    timetmvalue.tm_isdst = -1;
    timetmvalue.tm_gmtoff = 0;

    museconds = std::chrono::microseconds( std::stoi( stringtimepoint.substr( 15,6 ) ) );
    timettimepoint = timegm(&timetmvalue);

  } else {

    char* ptr_pos = strptime( stringtimepoint.c_str(), format.c_str(), &timetmvalue );

    // check if conversion worked.
    // If it did not return "zero" date.
    if ( ptr_pos == NULL ) {
      char datebuffer[20];
      timettimepoint = 0;
      timetmvalue = *std::gmtime( &timettimepoint );
      std::strftime ( datebuffer, sizeof(datebuffer), "%Y %m %d %H:%M:%S", &timetmvalue );
      std::cerr << "Error getting date and time. Setting everything to zero: \n\t"
          << "   "
          << stringtimepoint
          << "[" << format << "]\n\t"
          << "=> " << datebuffer
          << "[" << "%Y %m %d %H:%M:%S" << "]"
          << std::endl;
      return std::chrono::high_resolution_clock::from_time_t( timettimepoint );
    }// fi check date conversion


    // tm_isdst has to be set manually => set oracle mode for day light savings time
    timetmvalue.tm_isdst = -1;



    // save everything that was not parsed by strptime into a string
    std::string datarest( ptr_pos );
    if ( datarest.size() == 0 ) {
      // everything was parsed
      timettimepoint = timelocal( &timetmvalue );

    } else {
      // assuming the remaining part of the string contains
      // microseconds (.xyzetc)  and time zone
      std::stringstream ss;
      ss << datarest;

      double microseconds;
      std::string timezone;
      ss >> microseconds >> timezone;

      // set microseconds
      museconds = std::chrono::microseconds( static_cast<int>( microseconds*1000000 ) );

      // handle time zone (partially) and set it
      if ( timezone == "GMT" || timezone == "" ) {
        timettimepoint = timegm( &timetmvalue );
      } else {
        timettimepoint = timelocal( &timetmvalue );
      } // fi time zone


    } // fi unparsed data

  }// fi


  chronotimepoint = std::chrono::high_resolution_clock::from_time_t( timettimepoint );

  chronotimepoint += museconds;


#ifdef KSDEBUG
  std::cout << ">>out ::stringToChronoClock(string, string) " << std::endl;
#endif

  return chronotimepoint;
} // endof function TimeStamp::stringToChronoClock




//============================= ACCESS      ===================================
/** - none - **/

