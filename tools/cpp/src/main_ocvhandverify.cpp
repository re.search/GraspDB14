/** main_ocvshowmeasurehand - Displays the image of a hand along with the accompanying joint coordinates.
 *
 * #include "xx.h" <BR>
 * -llib tinyxml2
 * -llib opencv
 *
 * Loads an image and displays it along with the accompanying joint coordinates saved in a separate xml file.
 *
 * @see something
 *  Created on: Nov, 2014
 *      Author: kstoll2m
 *
 *
 *
 * Usage: ./<name-of-program>
 * press 'l' load another image (*-rect.jpg)
*        'm' to be able to measure pairs of points\n" <<
 *       'h' help (displays this information)
 *       <esc> end the program
 *
 */

#include <cstdlib>
#include <iostream>
#include <string>

#ifdef _WIN32
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <Windows.h>
#else
#include <opencv/highgui.h>
#include <opencv/cv.h>
#endif

#include "filechooser_class.h"
#include "filename_class.h"
/*#include "filestorageocv_class.h"*/
#include "filestoragexml_class.h"



// easy switching between float and double
typedef double real;
namespace cv {
  typedef Point2d Point2x;
  typedef Vec2d   Vec2x;
}



void loadRectifiedHandImage ( FileName&, cv::Mat&, double& );
void loadClickedPoints ( FileName& , std::vector<cv::Point2x >& );
void loadSegmentLengths( FileName&, std::vector< real >& );
void printHelp ( void );
void mouseCb ( int event, int x, int y, int flags, void* ptr );

struct PointPair {
  bool in_measure_mode;
  int  number_of_clicks;
  cv::Point2x p1;
  cv::Point2x p2;
  cv::Point2x current_location;
  double dist;
  double current_dist;

  PointPair( void ) {
    in_measure_mode = false;
    number_of_clicks = 0;
    dist = 0;
    current_dist = 0;
  }
  void reset( void ) {
    number_of_clicks = 0;
  }
};

double SCALEFACTOR;


int main ( int argc, char** argv ) {


  cv::Mat        image, originalimage;
  FileName       xmlfilename;
  bool           quit = false;
  bool           saved = false;
  std::string    bildgeladen;
  bool           in_measure_mode = false;


  // interaction related
  std::vector< cv::Point2x >            clickedpoints;
  std::vector< real >                   segmentlengths;

  bildgeladen = "No image could be loaded";

  loadRectifiedHandImage ( xmlfilename, image, SCALEFACTOR );
  bildgeladen = "Image name: " + xmlfilename.getName();
  if ( SCALEFACTOR > 0 ) {
    loadClickedPoints( xmlfilename, clickedpoints );
    loadSegmentLengths( xmlfilename, segmentlengths );
    originalimage = image.clone();
  }

  cv::namedWindow( bildgeladen, CV_WINDOW_NORMAL+CV_GUI_EXPANDED+CV_WINDOW_KEEPRATIO);
  //cv::setWindowProperty( bildgeladen, CV_WINDOW_FULLSCREEN, true );


  PointPair point_pair;
  cv::setMouseCallback( bildgeladen, mouseCb, &point_pair );

  printHelp();

  while ( !quit ) {
    // detect closing the window with the mouse (this will delete the mouse callback)
    if (!cvGetWindowHandle( bildgeladen.c_str() ) ) {
      cv::namedWindow( bildgeladen, CV_WINDOW_NORMAL+CV_GUI_EXPANDED+CV_WINDOW_KEEPRATIO);
      cv::setMouseCallback( bildgeladen, mouseCb, &point_pair );
    }

    if ( !image.empty()) {
#ifdef DRAW_POINTS_FROM_HANDMEASURE // for reproducing what the image would have looked like
    for ( size_t i=0; i<clickedpoints.size(); ++i ) {
        std::string text = std::to_string(26-i);//i+1);
        int fontface = cv::FONT_HERSHEY_PLAIN;
        double fontscale = 1.75;
        int thickness = 2;
        int baseline = 0;
        cv::Vec3b color = originalimage.at<cv::Vec3b>(clickedpoints[i]);
        cv::Scalar invcolor(255-color[0], 255-color[1], 255-color[2]);
        cv::Scalar black = cv::Scalar::all(0);
        cv::Scalar white(255,255,255,0);
        int radius = 3;

        if (i>0 /*&& i < 10*/ ) {
         // if ( i%2 )
          if ( i!=26-5 && i!=26-10 && i!=26-15 && i!=26-20 && i!=26-24 )
            cv::line ( image, clickedpoints[i-1], clickedpoints[i], black, 1 );
        }
        cv::circle ( image, clickedpoints[i], radius, invcolor );
        /*cv::circle ( image, center, radius, color, int thickness=1, int lineType=8, int shift=0 );*/
        cv::Size textsize = cv::getTextSize(text, fontface, fontscale, thickness, &baseline);
        baseline+=thickness;
        // add transparent box start
        cv::Rect roirect(clickedpoints[i]+cv::Point2x(10,baseline),clickedpoints[i]+cv::Point2x(10+textsize.width,-textsize.height-baseline));
        cv::Mat roi = image(roirect);
        cv::Mat roicolor(roi.size(), CV_8UC3, black);
        double alpha = 0.75;
        cv::addWeighted(roicolor, alpha, roi, 1.0-alpha, 0.0, roi);
        // add transparent box end
        cv::putText( image, text, clickedpoints[i]+cv::Point2x(10,0), fontface, fontscale, white, thickness);
      }
#else
      // draw lines
      for ( int i=1; i<clickedpoints.size(); ++i ) {
        cv::Scalar black = cv::Scalar::all(0);
        cv::Scalar white(255,255,255,0);
        int thickness = 3;
        int linetype = CV_AA;
        if ( i!=6 && i!=11 && i!=16 && i!=21 && i!=2) {
          // draw line segment
          cv::line ( image, clickedpoints[i-1], clickedpoints[i], white, thickness, linetype);
          cv::line ( image, clickedpoints[i-1], clickedpoints[i], black, thickness-1, linetype);

          // compute segment centre
          cv::Point2x segmentcentre = clickedpoints[i-1] + 0.5*(clickedpoints[i] - clickedpoints[i-1]);
          // compose string containing segment length rounded to one decimal digit
          std::string txt = std::to_string(static_cast< int >( (segmentlengths[i-1])*10 ) );
          txt.insert(txt.length()-1,".");
          txt += "mm";
          if (i!=2) {
            int fontface = cv::FONT_HERSHEY_PLAIN;
            double fontscale = 1;//1.5 for report
            int thickness = 1;//1.5 for report
            int baseline = 0;
            cv::Scalar black = cv::Scalar::all(0);
            cv::Scalar white(255,255,255,0);

            // add transparent box start
            cv::Size textsize = cv::getTextSize(txt, fontface, fontscale, thickness, &baseline);
            baseline+=thickness;
            cv::Rect roirect(segmentcentre+cv::Point2x(0,baseline/2),segmentcentre+cv::Point2x(textsize.width,-textsize.height-baseline/2));
            cv::Mat roi = image(roirect);
            cv::Mat roicolor(roi.size(), CV_8UC3, white);
            double alpha = 0.75;
            cv::addWeighted(roicolor, alpha, roi, 1.0-alpha, 0.0, roi);
            // add transparent box end
            // write into box
            cv::putText( image, txt, segmentcentre, fontface, fontscale, black, thickness, linetype );
          }
        } else {
          // these segments have not been measured
          //cv::line ( image, clickedpoints[1], clickedpoints[i], white, thickness, linetype);
          cv::line ( image, clickedpoints[1], clickedpoints[i], black, thickness-2, linetype);
        }

      }
      // draw dots
      for ( int i=0; i<clickedpoints.size(); ++i ) {
        cv::Vec3b color = originalimage.at<cv::Vec3b>(clickedpoints[i]);
        cv::Scalar black = cv::Scalar::all(0);
        cv::Scalar white(255,255,255,0);
        cv::Scalar invcolor(255-color[0], 255-color[1], 255-color[2]);
        cv::Scalar thecolor(color[0], color[1], color[2]);
        int radius = 6;
        int linetype = CV_AA;
        cv::circle ( image, clickedpoints[i], radius, white, -1, linetype);
        cv::circle ( image, clickedpoints[i], radius-1, black, -1, linetype);
        cv::circle ( image, clickedpoints[i], 1, invcolor, 1, linetype);
      }
      // draw labels
      for ( int i=0; i<clickedpoints.size(); ++i ) {
        std::string text = std::to_string(i);
        int fontface = cv::FONT_HERSHEY_PLAIN;
        double fontscale = 1.75;
        int thickness = 2;
        int baseline = 0;
        int linetype = CV_AA;
        cv::Scalar black = cv::Scalar::all(0);
        cv::Scalar white(255,255,255,0);

        // add transparent box start
        cv::Size textsize = cv::getTextSize(text, fontface, fontscale, thickness, &baseline);
        baseline+=thickness;
        cv::Rect roirect(clickedpoints[i]+cv::Point2x(10,baseline),clickedpoints[i]+cv::Point2x(10+textsize.width,-textsize.height-baseline));
        cv::Mat roi = image(roirect);
        cv::Mat roicolor(roi.size(), CV_8UC3, black);
        double alpha = 0.75;
        cv::addWeighted(roicolor, alpha, roi, 1.0-alpha, 0.0, roi);
        // add transparent box end
        cv::putText( image, text, clickedpoints[i]+cv::Point2x(10,0), fontface, fontscale, white, thickness, linetype);
      }
#endif

      if ( point_pair.in_measure_mode && (point_pair.number_of_clicks > 0) ) {
        if ( (point_pair.number_of_clicks%2) == 1 ) {
          cv::circle ( image, point_pair.p1, 4, cv::Scalar::all(255), -1, CV_AA);
          cv::line( image, point_pair.p1, point_pair.current_location, cv::Scalar::all(255), 1, CV_AA );
          cv::putText( image, std::to_string(point_pair.current_dist), point_pair.current_location, cv::FONT_HERSHEY_PLAIN, 1, cv::Scalar::all(255), 1, CV_AA );
        } else {
          cv::circle ( image, point_pair.p1, 4, cv::Scalar::all(255), -1, CV_AA);
          cv::circle ( image, point_pair.p2, 4, cv::Scalar::all(255), -1, CV_AA);
          cv::line( image, point_pair.p1, point_pair.p2, cv::Scalar::all(255), 1, CV_AA );
          cv::putText( image, std::to_string(point_pair.dist), point_pair.p1, cv::FONT_HERSHEY_PLAIN, 1, cv::Scalar::all(255), 1, CV_AA );
        }
      }

      cv::imshow ( bildgeladen, image );

    }




    char k = cv::waitKey(5);
    switch (k) {
    case 27 :
      quit = true;
      break;
    case 'l' :
      loadRectifiedHandImage ( xmlfilename, image, SCALEFACTOR );
      if ( SCALEFACTOR > 0 ) {
        originalimage = image.clone();
        clickedpoints.clear();
        loadClickedPoints( xmlfilename, clickedpoints );
        loadSegmentLengths( xmlfilename, segmentlengths );
        in_measure_mode = false;
        point_pair.in_measure_mode = in_measure_mode;
        std::cout << "measure mode is set to " << (in_measure_mode ? "ON" : "OFF") << std::endl;
        point_pair.reset();
      }
      break;
    case 'm' :
      in_measure_mode = !in_measure_mode;
      point_pair.in_measure_mode = in_measure_mode;
      std::cout << "measure mode is now " << (in_measure_mode ? "ON" : "OFF") << std::endl;
      if ( !in_measure_mode ) {
        point_pair.reset();
      }
      break;
    case 'h' :
      printHelp();
      break;
    case 'i' :
      //std::string fn = xmlfilename.getFileName();
      cv::imwrite( xmlfilename.getFileName()+"-verfication.png", image ); 
      break;
    default:
      break;
    }

    image = originalimage.clone();
}

  cv::destroyAllWindows();

  return EXIT_SUCCESS;

} // endof main








//-----------------------------------------------------------------------------
void 
printHelp ( void ) {
  
  std::cout <<
    "\n" <<
    "  You asked for help so help shall be provided.\n" <<
    "    There are the following ways of interaction: \n" <<
    "    Press 'l' to load another image (*-rect.jpg)\n" <<
    "          'm' to be able to measure pairs of points\n" <<
    "          'h' to help (displays this information)\n" <<
    "          <esc> to end the program\n" <<
    std::endl;

} // endof printHelp






//-----------------------------------------------------------------------------
void
loadRectifiedHandImage ( FileName& ref_filename, cv::Mat& ref_image, double& ref_scalefactor ) {

  FileChooser    imagename;
  std::string    imagetoload;


  imagename.setDefaultDirectory("$HOME/");
  imagename.chooseFile("-rect.jpg");
  imagetoload = imagename.getFile();
//  imagetoload = "/home/kstoll2m/PhD/native/subject01/img20140911155734-hand-rect.jpg";
  if ( imagetoload.empty() ) {
    ref_scalefactor = -1;
    printHelp();
    return;
  }
  ref_filename = FileName(imagetoload);
  ref_filename.setExtension("xml");

  FileStorageXml xmldoc( ref_filename.getCompleteFileName() );
  ref_scalefactor = xmldoc.loadHomographyScale( ref_filename.getCompleteFileName() );

  ref_image = cv::imread( imagetoload );

} // endof loadRectifiedHandImage





//-----------------------------------------------------------------------------
void
loadClickedPoints ( FileName& ref_filename, std::vector<cv::Point2x >& ref_clickedpoints ) {

  std::cout << "loading points" << std::endl;
/*
  cv::FileStorage ocvfs;
  cv::FileNode    ocvfn;


  // write everything to the (already existant) xml-file
  ocvfs.open(ref_filename.getCompleteFileName(), cv::FileStorage::READ);

  ocvfn = ocvfs["hand"]["right"]["image_based_measurement"]["raw"];
  ocvfn["coordinates_2d"] >> ref_clickedpoints;
  ocvfs.release();
*/


  ref_clickedpoints.clear();
  std::vector < real > temp;
  temp.clear();

  FileStorageXml xmldoc( ref_filename.getCompleteFileName() );

  xmldoc.loadHand2dCoordinates( temp, "", RAW+FROMIMG );


  if ( temp.size() ) {
    cv::Point2x p;
    for ( int i=0; i<temp.size(); i+=2 ) {
      p.x = temp[i];
      p.y = temp[i+1];
      ref_clickedpoints.push_back(p);
    }
  }


  /* joints (clicked order -idxB- and namemap -idxA-)
   *
   * idxB | idxA | name     | description
   * -----+------+----------+----------------------------------------
   *   25 |    0 | -------- | not present
   *   24 |    1 | root     | root / wrist
   *   23 |    2 | th-tmcj  | Thumb  trapeziometacarpal joint
   *   22 |    3 | th-mcpj  | Thumb  metacarpal joint
   *   21 |    4 | th-ipj   | Thumb  interphalangeal joint
   *   20 |    5 | th-tip   | Thumb  tip
   *   19 |    6 | if-cmcj  | Index  carpometacarpal joint
   *   18 |    7 | if-mcpj  | Index  metacarpal joint
   *   17 |    8 | if-pipj  | Index  proximal interphalangeal joint
   *   16 |    9 | if-dipj  | Index  distal interphalangeal joint
   *   15 |   10 | if-tip   | Index  tip
   *   14 |   11 | mf-cmcj  | Middle carpometacarpal joint
   *   13 |   12 | mf-mcpj  | Middle metacarpal joint
   *   12 |   13 | mf-pipj  | Middle proximal interphalangeal joint
   *   11 |   14 | mf-dipj  | Middle distal interphalangeal joint
   *   11 |   15 | mf-tip   | Middle tip
   *    9 |   16 | rf-cmcj  | Ring   carpometacarpal joint
   *    8 |   17 | rf-mcpj  | Ring   metacarpal joint
   *    7 |   18 | rf-pipj  | Ring   proximal interphalangeal joint
   *    6 |   19 | rf-dipj  | Ring   distal interphalangeal joint
   *    5 |   20 | rf-tip   | Ring   tip
   *    4 |   21 | lf-cmcj  | Little carpometacarpal joint
   *    3 |   22 | lf-mcpj  | Little metacarpal joint
   *    2 |   23 | lf-pipj  | Little proximal interphalangeal joint
   *    1 |   24 | lf-dipj  | Little distal interphalangeal joint
   *    0 |   25 | lf-tip   | Little tip
   */

  /* bones
   *   1 | root   | root / wrist
   *   2 | th-c   | Thumb  carpal
   *   3 | th-mc  | Thumb  metacarpal
   *   4 | th-pp  | Thumb  proximal phalanx
   *   5 | th-dp  | Thumb  distal phalanx
   *   6 | if-c   | Index  carpal
   *   7 | if-mc  | Index  metacarpal
   *   8 | if-pp  | Index  proximal phalanx
   *   9 | if-mp  | Index  middle phalanx
   *  10 | if-dp  | Index  distal phalanx
   *  11 | mf-c   | Middle carpal
   *  12 | mf-mc  | Middle metacarpal
   *  13 | mf-pp  | Middle proximal phalanx
   *  14 | mf-mp  | Middle middle phalanx
   *  15 | mf-dp  | Middle distal phalanx
   *  16 | rf-c   | Ring   carpal
   *  17 | rf-mc  | Ring   metacarpal
   *  18 | rf-pp  | Ring   proximal phalanx
   *  19 | rf-mp  | Ring   middle phalanx
   *  20 | rf-dp  | Ring   distal phalanx
   *  21 | lf-c   | Little carpal
   *  22 | lf-mc  | Little metacarpal
   *  23 | lf-pp  | Little proximal phalanx
   *  24 | lf-mp  | Little middle phalanx
   *  25 | lf-dp  | Little distal phalanx
   */

} // endof saveClickedPoints



//-----------------------------------------------------------------------------
void 
loadSegmentLengths( FileName& ref_filename, std::vector< real >& ref_segmentlengths ) {
  std::cout << "loading segment lengths" << std::endl;

  ref_segmentlengths.clear();
  //std::vector < real > temp;
  //temp.clear();

  FileStorageXml xmldoc( ref_filename.getCompleteFileName() );

  xmldoc.loadHandLengths( ref_segmentlengths, "", FROMIMG );

}

//-----------------------------------------------------------------------------
void
mouseCb ( int event, int x, int y, int flags, void* ptr ) {

  PointPair* ptr_pointpair = (PointPair *) ptr;
  float dist = 0;

  if (event == cv::EVENT_FLAG_LBUTTON ) {

    if ( ptr_pointpair->in_measure_mode ) {
      ptr_pointpair->number_of_clicks++;
      if ( (ptr_pointpair->number_of_clicks%2) == 1 ) {
        ptr_pointpair->p1.x = x;
        ptr_pointpair->p1.y = y;
      } else if ( (ptr_pointpair->number_of_clicks>0) && ( (ptr_pointpair->number_of_clicks%2) == 0 ) ) {
        ptr_pointpair->p2.x = x;
        ptr_pointpair->p2.y = y;

        cv::Vec2x p1( ptr_pointpair->p1.x, ptr_pointpair->p1.y );
        cv::Vec2x p2( ptr_pointpair->p2.x, ptr_pointpair->p2.y );
        dist = cv::norm( p1, p2, CV_L2 );

        ptr_pointpair->dist = dist/SCALEFACTOR*1.;
        ptr_pointpair->current_dist = ptr_pointpair->dist;

        std::cout << "d([" << /*std::setw(4) <<*/  p2[0] << ", " << /*std::setw(4) <<*/ p2[1] << "]"
          << ",[" << /*std::setw(4) <<*/  p1[0] << ", " << /*std::setw(4) <<*/ p1[1] << "])"
            << /*std::setw(6) <<*/ dist << "/" << SCALEFACTOR << "="
            << dist/SCALEFACTOR*1. << std::endl;
      }
    } // fi in_measure_mode

  } else {
    ptr_pointpair->current_location.x = x;
    ptr_pointpair->current_location.y = y;

    cv::Vec2x p1( ptr_pointpair->p1.x, ptr_pointpair->p1.y );
    cv::Vec2x p2( ptr_pointpair->current_location.x, ptr_pointpair->current_location.y );
    dist = cv::norm( p1, p2, CV_L2 );

    ptr_pointpair->current_dist = dist/SCALEFACTOR*1.;

  }
} // endof mouseCb





