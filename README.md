# Content

This repository contains the database and accompanying matlab and c++ code as described in "GraspDB14 – Documentation on a database of grasp motions and its creation". The technical report is available through [https://doi.org/10.18418/978-3-96043-061-2](https://doi.org/10.18418/978-3-96043-061-2).

The presented database GraspDB14 in sum contains over 2000 prehensile movements of ten different non-professional actors interacting with 15 different objects. Each grasp was realised five times by each actor. The motions are systematically named containing an (anonymous) identifier for each actor as well as one for the object grasped or interacted with. The data were recorded as joint angles (and raw 8-bit sensor data) which can be transformed into positional 3D data (3D trajectories of each joint). In the report, we provide a detailed description on the GraspDB14-database as well as on its creation (for reproducibility). 

The database was partially annotated and used for automatic segmentation of grasp motions. The methods were published in 2016 and can be found at [here](http://vc.inf.h-bonn-rhein-sieg.de/basilic/Publications/2016/SVKKH16/).